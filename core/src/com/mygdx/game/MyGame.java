package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Iterator;


public class MyGame extends ApplicationAdapter{

    private Stage stage;
    private OrthographicCamera cam;
    private Viewport viewport;
    public static int SCREENWIDTH;
    public static int SCREENHEIGHT;
    public static float RATIO ;
    private SpriteBatch batch;
    private BitmapFont labelFont;
    private Label fpsInfo,scoreInfo;
    private int fps;
    private Image killer;
    private TextureAtlas images;
    private Array<Weapon> weaponArray;
    private Pool<Weapon> weaponPool;
    private Group monster;
    private long score;

    public void initMain(){
        cam = new OrthographicCamera();
        SCREENWIDTH = Gdx.graphics.getHeight();
        SCREENHEIGHT = Gdx.graphics.getWidth();
        RATIO = SCREENHEIGHT/SCREENWIDTH;
        viewport = new FitViewport(400,400*RATIO,cam);
        cam.translate(400 / 2, 400 * RATIO / 2);
        cam.update();
        batch = new SpriteBatch();
        stage = new Stage(viewport,batch);
        Gdx.input.setInputProcessor(stage);
        images = new TextureAtlas(Gdx.files.internal("object/KillMonster.atlas"));

    }

    public void initScore(){
        labelFont = new BitmapFont(Gdx.files.internal("font/labelFont.fnt"),Gdx.files.internal("font/labelFont.png"),false);
        Label.LabelStyle style = new Label.LabelStyle(labelFont,labelFont.getColor());
        scoreInfo = new Label("Score:",style);
        scoreInfo.setY(SCREENHEIGHT-scoreInfo.getHeight());
        scoreInfo.setX(SCREENWIDTH / 2 - scoreInfo.getWidth() / 2);
        stage.addActor(scoreInfo);
        scoreInfo.setName("scoreInfo");
        score = 0;
    }

    public void initFpsInfo(){
        labelFont = new BitmapFont(Gdx.files.internal("font/labelFont.fnt"),Gdx.files.internal("font/labelFont.png"),false);
        Label.LabelStyle style = new Label.LabelStyle(labelFont,labelFont.getColor());
        fpsInfo = new Label("FPS:",style);
        fpsInfo.setY(0);
        stage.addActor(fpsInfo);
        fpsInfo.setName("fpsInfo");
    }

    public void initKiller(){
        killer = new Image(images.findRegion("killer"));
        killer.setPosition(0, SCREENHEIGHT / 2 - killer.getHeight() / 2);
        stage.addActor(killer);
    }

    public void iniMonster(){
        monster = new MonsterGroup(images.findRegion("monster"),4);
        stage.addActor(monster);
    }

    class Input extends InputAdapter{
        @Override
        public boolean keyUp(int keycode) {
            switch (keycode){
                case com.badlogic.gdx.Input.Keys.UP:
                    killer.setPosition(killer.getX(),killer.getY()+Gdx.graphics.getDeltaTime()*1000);
                    break;
                case com.badlogic.gdx.Input.Keys.DOWN:
                    killer.setPosition(killer.getX(),killer.getY()-Gdx.graphics.getDeltaTime()*1000);
                    break;
                case com.badlogic.gdx.Input.Keys.A:
                    Weapon item = weaponPool.obtain();
                    item.init();
                    weaponArray.add(item);
                    stage.addActor(item);
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public void initWeapon(){
        weaponArray = new Array<Weapon>();
        weaponPool = new Pool<Weapon>() {
            @Override
            protected Weapon newObject() {
                return new Weapon(images.findRegion("weapon"),killer);
            }
        };
    }

    @Override
    public void create() {
        initMain();
        initFpsInfo();
        initScore();
        initKiller();
        iniMonster();
        initWeapon();
        Gdx.input.setInputProcessor(new Input());
    }

    @Override
    public void dispose() {
        batch.dispose();
        stage.dispose();
    }

    @Override
    public void render() {
        cam.update();
        Gdx.gl20.glClearColor(1, 1, 1, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        stage.act();
        stage.draw();

        fps = Gdx.graphics.getFramesPerSecond();
        Label fpsLabel = stage.getRoot().findActor("fpsInfo");
        fpsLabel.setText("Fps:" + fps);
        fpsLabel.setX(400 - fpsLabel.getPrefWidth());

        Label scoreLabel = stage.getRoot().findActor("scoreInfo");
        scoreLabel.setText("Score:" + score);

        if(killer.getY()<0){
            killer.setY(0);
        }
        if(killer.getY()>MyGame.SCREENHEIGHT- killer.getHeight()){
            killer.setY(MyGame.SCREENHEIGHT- killer.getHeight());
        }
        Iterator<Weapon> iter = weaponArray.iterator();
        while(iter.hasNext()){
            Weapon weapon = iter.next();
            weapon.update();
            if(weapon.getX()>MyGame.SCREENWIDTH-weapon.getWidth()){
                weaponPool.free(weapon);
                iter.remove();
            }
            else{
                for(Actor actor:monster.getChildren()){
                    if(weapon.getX()>=actor.getX()&&weapon.getY()>=actor.getY()&&weapon.getX()<=actor.getX()+actor.getWidth()&&weapon.getY()<=actor.getY()+actor.getHeight()){
                        iter.remove();
                        score++;
                        weaponPool.free(weapon);
                        monster.removeActor(actor);
                        break;
                    }
                }
            }
        }
        if(Gdx.input.isKeyPressed(com.badlogic.gdx.Input.Keys.ENTER)){
            Gdx.app.log("weaponpeak",weaponPool.peak+"");
            Gdx.app.log("weaponArray",weaponArray.size+"");
        }
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width,height);
    }
}
