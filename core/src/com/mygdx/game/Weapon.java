package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by Administrator on 2016/2/23.
 */
public class Weapon extends Actor implements Pool.Poolable{

    private TextureRegion image;
    private boolean isAlive;
    private Actor actor;

    public Weapon(TextureRegion image,Actor actor){
        this.image = image;
        this.actor = actor;
    }

    public void init(){
        isAlive = true;
        this.setX(actor.getX()+actor.getWidth());
        this.setY(actor.getY()+actor.getHeight()/2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(isAlive==true){
            batch.draw(image,this.getX(),this.getY());
        }
    }

    @Override
    public void reset() {
        isAlive = false;
    }

    public void update(){
        if(isAlive){
            if(this.getX()<MyGame.SCREENWIDTH-this.getWidth()){
                this.setX(this.getX()+Gdx.graphics.getDeltaTime()*100);
            }
        }
    }
}
